const bitcoin = require('bitcoinjs-lib')
const TESTNET = bitcoin.networks.testnet
const LITECOIN = {
    messagePrefix: '\x19Litecoin Signed Message:\n',
    bip32: {
      public: 0x019da462,
      private: 0x019d9cfe
    },
    pubKeyHash: 0x30,
    scriptHash: 0x32,
    wif: 0xb0
  }
//import nodeUrl from '../eth-node-config.json';
//import Web3 from 'web3';


/**
 * Create web3 instance
 */
//const web3 = new Web3(nodeUrl.url);

let domBitcoin = null;
domBitcoin = document.createElement("div");
domBitcoin.className = "domBitcoin";
document.body.appendChild(domBitcoin);

let img = new Image();
img.src = '../src/images/b.png';
const elem = document.createElement('canvas');
elem.width = 10;
elem.height = 10;
const ctx = elem.getContext('2d');
ctx.drawImage(img, 0, 0, 5, 5);
domBitcoin.appendChild(img);

let button1 = document.createElement("button");
domBitcoin.appendChild(button1);
button1.innerHTML = "Generate random Bitcoin address";
button1.onclick = () => {
    let keyPair = bitcoin.ECPair.makeRandom({ network: TESTNET });
    const { address } = bitcoin.payments.p2pkh({ pubkey: keyPair.publicKey, network: TESTNET })
    let publicKey = address;
    let privateKey = keyPair.toWIF();
    User1.address = publicKey;
    User1.privateKey = privateKey;
    labela1.innerHTML = "User1 address: " + User1.address + " , private key: " + User1.privateKey;
    console.log(`Public: ${publicKey} \n Private: ${privateKey}`);
}

let User1 = {
    address: "",
    privateKey: "",
    balance: 0
};

let User2 = {
    address: "",
    privateKey: "",
    balance: 0
};

let labela1 = document.createElement("label");
labela1.innerHTML = "User1 address: " + User1.address + " , private key: " + User1.privateKey;
domBitcoin.appendChild(labela1);



let button2 = document.createElement("button");
document.body.appendChild(button2);
button2.innerHTML = "Generate random Litecoin address";
button2.onclick = () => {
    const keyPair = bitcoin.ECPair.makeRandom({ network: LITECOIN })
    const { address } = bitcoin.payments.p2pkh({ pubkey: keyPair.publicKey, network: LITECOIN })
    let publicKey = address;
    let privateKey = keyPair.toWIF();
    User2.address = publicKey;
    User2.privateKey = privateKey;
    labela2.innerHTML = "User2 address: " + User2.address + " , private key: " + User2.privateKey;
    console.log(`Public: ${publicKey} \n Private: ${privateKey}`);
}

let labela2 = document.createElement("label");
labela2.innerHTML = "User2 address: " + User2.address + " , private key: " + User2.privateKey;
document.body.appendChild(labela2);

//function generateRandomEthereumAddress() {
  //  return new Promise((resolve, reject) => {
    //    resolve(web3.eth.personal.newAccount('koliko'));
    //})
//}

/*let button3 = document.createElement("button");
document.body.appendChild(button3);
button3.innerHTML = "Generate random Ethereum address";
button3.onclick = () => {
    generateRandomEthereumAddress()
    .then(address => console.log(address));
}*/


let shoppingChartDiv = document.createElement("div");
document.body.appendChild(shoppingChartDiv);

let concreteProductDiv = document.createElement("div");

let concreteProductNameDiv = document.createElement("div");
let concreteProductNameLabel = document.createElement("label");
concreteProductNameLabel.innerHTML = "Nike Air Force 1.0";
concreteProductNameDiv.appendChild(concreteProductNameLabel);

let concreteProductPriceDiv = document.createElement("div");
let concreteProductPriceLabel = document.createElement("label");
concreteProductPriceLabel.innerHTML = "1 BTC / 1.2 LTC";
concreteProductPriceDiv.appendChild(concreteProductPriceLabel);

let addToChartButton = document.createElement("button");
addToChartButton.innerHTML = "Add to Chart";
addToChartButton.onclick = (e) => {
    alert("Added to chart");
}


concreteProductDiv.appendChild(concreteProductNameDiv);
concreteProductDiv.appendChild(concreteProductPriceDiv);
concreteProductDiv.appendChild(addToChartButton);

shoppingChartDiv.appendChild(concreteProductDiv);